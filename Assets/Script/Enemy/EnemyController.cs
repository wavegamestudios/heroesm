﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для работы с противниками
/// </summary>
public class EnemyController : SingletonBehaviour<EnemyController>
{
    private ArrayList enemyPosList = new ArrayList() { EnemyMap.enemyPos1, EnemyMap.enemePos2, EnemyMap.enemePos3 };
    public List<Enemy> enemyList = new List<Enemy>();

    private void Start() => LoadEnemy();
    
    private void LoadEnemy()
    {
        for (int i = 0; i <= enemyList.Count-1; i++)
        {
            Enemy EnemyInstance = Instantiate<Enemy>(enemyList[i]);
            EnemyInstance.transform.SetParent(Helper.Instance.GetCellToPosition((Vector2)enemyPosList[i]).transform, false);
            EnemyInstance.transform.localPosition = Vector2.zero;
        }
    }
}
