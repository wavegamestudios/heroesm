﻿using DG.Tweening;
using UnityEngine;

/// <summary>
/// Логика поведения противника
/// </summary>
public class Enemy : MonoBehaviour
{
    HexCell parentCell { get; set; }

    void Start() => PathController.Instance.OnPlayerEndPatn += CheckAttack;
   
    /// <summary>
    /// Подвинуть потивника и проверить клетку игрока на атаку
    /// </summary>
    private void CheckAttack()
    {    
        DOVirtual.DelayedCall(0.5f, () =>
        {
            if (!SaveManager.Instance.armyDeathList.Contains(GetComponent<HeroConfig>().ArmyName))
            {
                MoveEnemy();
                CheckAttackPosition();
            }
        });
    }

    /// <summary>
    /// Поиск игрока на клетках рядом
    /// </summary>
    private void MoveEnemy()
    {
        var neigbors = GetCellParentTransform().GetComponent<HexCell>().GetAllNeighbor();

        foreach (var neighbor in neigbors)
        {
            foreach (Transform child in neighbor.transform)
            {
                if (child.transform.GetComponent<Player>())
                {
                    Move(neighbor);
                }
            }
        }
    }

    private void CheckAttackPosition()
    {
        Transform playerTr = PlayerController.instance.playerInstance.GetCellParentTransform();

        if (playerTr == GetCellParentTransform())
        {  
            AttackController.Instance.LoadAttack(PlayerController.instance.playerInstance, this);
        }
    }

    //Ход на клетку игрока
    public void Move(HexCell hexCell)
    {
        transform.SetParent(hexCell.transform);
        transform.DOMove(hexCell.transform.position, 0.4f);
    }

    public Transform GetCellParentTransform() => transform.parent;
}
