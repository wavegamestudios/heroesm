﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Контроллер путей
/// </summary>
public class PathController : Singleton<PathController>
{
    private Transform PlayerParent() => PlayerController.instance.playerInstance.GetCellParentTransform();
    public Action OnPlayerEndPatn;
    public HexCell lastSelectedCell { get; set; }

    //Ограничить клики пока не пройден путь
    public bool blockButton = true;

    private Queue GetPath(HexCell selectedHex)
    {
        HexCell playerHex = PlayerParent().GetComponent<HexCell>();
        return FindRow.Instance.GetCellPath(playerHex, selectedHex);
    }

    public void SelectedPath(HexCell selectedCell)
    {
        var queueCell = GetPath(selectedCell);

        foreach (HexCell cell in queueCell)
        {
            cell.SelectedCell();
        }

        lastSelectedCell = selectedCell;
    }

    public void DeSelectedPath()
    {
        foreach (HexCell cell in SceneMatrix.Instance.cells)
        {
            cell.DeSelectedCell();
        }
    }


    public void MoveLastSelectedCell()
    {
        if (lastSelectedCell != null && blockButton)
        {
            MovePlayerToPath(lastSelectedCell);
            blockButton = false;
        }
    }

    public void MovePlayerToPath(HexCell selectedHex)
    {
        Queue queueCell = GetPath(selectedHex);
        DOTween.Sequence().Append(DOVirtual.DelayedCall(0.2f, () => { PlayerController.instance.playerInstance.MovePlayer(queueCell.Dequeue() as HexCell); }).SetLoops(queueCell.Count)).OnComplete(() =>
        {
            DeSelectedPath();
            OnPlayerEndPatn();
            lastSelectedCell = null;
            blockButton = true;
        });
    }
}
