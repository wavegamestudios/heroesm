﻿using Graphs;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Поиск путь от точки до точки (поиск в ширину)
/// </summary>
public class FindRow : Singleton<FindRow>
{
    //Узлы графа карты
    ArrayList mapGraph { get; set; }

    public FindRow() => InitGraphNode();
    
    //Создаем граф карты
    private void InitGraphNode()
    {
        mapGraph = new ArrayList();

        //Заполняем таблицу нод
        foreach(HexCell node in SceneMatrix.Instance.cells)
        {
            mapGraph.Add(new Node(node.transform.name));  
        }

        //Добавляем всех родствеников в узлы
        foreach (HexCell cellNode in SceneMatrix.Instance.cells)
        {
            var n = FindNode(cellNode.transform.name);

            foreach (var Neighbor in cellNode.GetAllNeighbor())
            {
                var n1 = FindNode(Neighbor.transform.name);
                n.AddChildren(n1);
            }
        }
    }

    //Получит список нод пути
    private LinkedList<Node> GetNodePath(HexCell startPath, HexCell endPath)
    {
        var nStart = FindNode(startPath.transform.name);
        var nEnd = FindNode(endPath.transform.name);
        var search = new DepthFirstSearch();
        var path = search.IDDFS(nStart, nEnd);
        return path;
    }

    //Получить список клеток пути
    public Queue GetCellPath(HexCell startPath, HexCell endPath)
    {
        Queue queueCell = new Queue();
        LinkedList<Node> nodeList = GetNodePath(startPath, endPath);

        foreach (var node in nodeList)
        {
            HexCell cell = (from HexCell hexCell in SceneMatrix.Instance.cells where hexCell.transform.name == node.Name select hexCell).First();
            queueCell.Enqueue(cell);
        }

        return queueCell;
    }

    private Node FindNode(string nodeName) => (from Node node in mapGraph where node.Name == nodeName select node).First();   
}

