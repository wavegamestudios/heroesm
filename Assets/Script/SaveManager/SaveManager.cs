﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class UnitSavedField
{
    public string unitName { get; set; }
    public int unitValue { get; set; }
}

/// <summary>
/// Сохранять данные пользователя
/// </summary>
public class SaveManager : Singleton<SaveManager>
{
    public HashSet<string> armyDeathList = new HashSet<string>();

    public List<UnitSavedField> playerUnitList = new List<UnitSavedField>();

    public void SavePlayerUnits(List<Unit> playerList)
    {
        playerUnitList = new List<UnitSavedField>();

        foreach (Unit player in playerList)
        {
            UnitSavedField unit = new UnitSavedField();
            unit.unitName = player.unitName;
            unit.unitValue = player.unitValue;
            playerUnitList.Add(unit);
        }
    }

    public List<UnitSavedField> GetPlayerUnits()
    {
        return playerUnitList;
    }
}
