﻿using UnityEngine;

/// <summary>
/// Матрица карты дорого (0 - нет прохода, в остальном любая цифра)
/// </summary>
public struct MatrixMapRoad
{
    public const int height = 11;
    public const int width = 19;

    public static int[,] roadMap = new int[height, width]
    {
       {0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
       {0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0},
       {0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,1,0,0,0},
       {0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0},
       {0,0,0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,0,0},
       {0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0},
       {0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,1,0,0},
       {0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,1,0,0},
       {0,0,1,0,0,0,0,0,0,0,1,0,0,1,0,0,0,1,0},
       {0,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1},
       {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
    };
}

/// <summary>
/// Первая  позиция с которой игрок начинаем игру
/// </summary>
public struct CharacterMap
{
    public static Vector2 firstCharacterPos = new Vector2(0, 2);
}

/// <summary>
/// Позиции с которой стартует вражеская армия
/// </summary>
public struct EnemyMap
{
    public static Vector2 enemyPos1 = new Vector2(0, 10);
    public static Vector2 enemePos2 = new Vector2(4, 13);
    public static Vector2 enemePos3 = new Vector2(7, 3);
}
