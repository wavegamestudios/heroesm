﻿using System.Collections;

/// <summary>
/// Структура армии
/// </summary>
public struct Army
{
    public string unitName { get; }
    public int unitValue { get; set; }

    public Army(string unitName, int unitValue)
    {
        this.unitName = unitName;
        this.unitValue = unitValue;
    }
}

/// <summary>
/// Структура конфига юнита
/// </summary>
public struct WarriorSkills
{
    public int hp { get; }
    public int action { get; }
    public ActiveSkillEn science { get; }

    public WarriorSkills(int hp, int action, ActiveSkillEn science)
    {
        this.hp = hp;
        this.action = action;
        this.science = science;
    }
}

public class ArmyConfig : Singleton<ArmyConfig>
{
    //Army1, Army2, Army3, Army4
    public Hashtable ArmyTable = new Hashtable();

    //Archers, Pikemen, Knights, Goblins, 
    public Hashtable WarriorsSkillTable = new Hashtable();

    public ArmyConfig()
    {
        InitArmy();
        InitWarriors();
    }

    /// <summary>
    /// Загрузить данные о юнитах
    /// </summary>
    private void InitWarriors()
    {
        //Колличество жизней, уровень атаки
        WarriorsSkillTable.Add("Pikemen", new WarriorSkills(350, 100, ActiveSkillEn.none));
        WarriorsSkillTable.Add("Archers", new WarriorSkills(100, 100, ActiveSkillEn.defeat));
        WarriorsSkillTable.Add("Knights", new WarriorSkills(700, 250, ActiveSkillEn.healing));
        WarriorsSkillTable.Add("Goblins", new WarriorSkills(250, 120, ActiveSkillEn.frustration));
    }

    /// <summary>
    /// Загрузить армии
    /// </summary>
    private void InitArmy()
    {
        //Название война, колличество в отряде (Army1 армия игрока )
        ArmyTable.Add("Army1", new ArrayList { new Army("Archers", 1), new Army("Archers", 2), new Army("Knights", 10), new Army("Goblins", 10) });

        //Армий противника
        ArmyTable.Add("Army2", new ArrayList { new Army("Archers", 10), new Army("Pikemen", 5), new Army("Goblins", 10) });
        ArmyTable.Add("Army3", new ArrayList { new Army("Knights", 5), new Army("Goblins", 5) });
        ArmyTable.Add("Army4", new ArrayList { new Army("Knights", 5), new Army("Pikemen", 20) });
    }
}
