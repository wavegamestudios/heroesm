﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Инициализатор карты боя (загрузка игрока и противников на карту)
/// </summary>
public class WarMap : SingletonBehaviour<WarMap>
{
    public GameObject playerPanel;
    public GameObject enemyPanel;

    public string EnemyArmyName { get; set; }

    public static Action selectedPlayer;

    public void InitMap(Player player, Enemy enemy)
    {
        List<UnitSavedField> playerUnitList = SaveManager.Instance.GetPlayerUnits();

        LoadArmy(enemy.GetComponent<HeroConfig>().ArmyName, enemyPanel, UnitSide.EnemySide);
        EnemyArmyName = enemy.GetComponent<HeroConfig>().ArmyName;

        if (playerUnitList.Count <= 0)
        {
            LoadArmy(player.GetComponent<HeroConfig>().ArmyName, playerPanel, UnitSide.PlayerSide);
        }
        else
        {
            LoadSaveUnit(playerUnitList, playerPanel, player.GetComponent<HeroConfig>().ArmyName);
        }

        DOVirtual.DelayedCall(2.0f, () =>
        {
            selectedPlayer?.Invoke();
        });
    }

    private void LoadSaveUnit(List<UnitSavedField> playerUnitList, GameObject panel, string armyName)
    {
        var armyTable = ArmyConfig.Instance.ArmyTable;
        var army = (ArrayList)armyTable[armyName];

        foreach (Army unit in army)
        {
            var oldUnit = playerUnitList.Find(r => r.unitName == unit.unitName);

            if (oldUnit != null)
            {
                UnitInstance(unit.unitName, oldUnit.unitValue, panel, UnitSide.PlayerSide, armyName);
                playerUnitList.Remove(oldUnit);
            }
        }
    }

    private void LoadArmy(string armyName, GameObject panel, UnitSide unitSide)
    {
        var armyTable = ArmyConfig.Instance.ArmyTable;
        var army = (ArrayList)armyTable[armyName];

        foreach (Army unit in army)
        {
            UnitInstance(unit.unitName, unit.unitValue, panel, unitSide, armyName);
        }
    }

    private void UnitInstance(string unitName, int unitValue, GameObject panel, UnitSide unitSide, string armyName)
    {
        var unitList = UnitControllers.instance.UnitList;
        var unit = unitList.Find(r => r.name == unitName);
        Unit unitInstance = Instantiate(unit);
        unitInstance.transform.SetParent(panel.transform, false);
        unitInstance.transform.localScale = new Vector3(1, 1, 1);
        unitInstance.unitValue = unitValue;
        unitInstance.unitName = unitName;
        unitInstance.unitSide = unitSide;
        unitInstance.armyName = armyName;
        unitInstance.useSkill = false;
    }
}
