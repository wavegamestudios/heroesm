﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillsActivator : MonoBehaviour, IPointerClickHandler
{
    public static Action CheckAfterSkills;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (AttackHelper.Instance.GetAllPlayerLive().Count > 0)
        {
            Unit unit = transform.parent.GetComponent<Unit>();
            AttackHelper.Instance.ActivateSkills(unit);
            CheckAfterSkills?.Invoke();
        }
    }
}
