﻿using UnityEngine;


/// <summary>
/// Обработчик активных скилов
/// </summary>
public abstract class Strategy
{
    public virtual void AlgorithmInterface(Unit unit) { }
    public virtual void AlgorithmInterface() { }
}

public class Context
{
    Strategy strategy;

    public Context(Strategy strategy)
    {
        this.strategy = strategy;
    }

    public void ContextInterface(Unit unit)
    {
        strategy.AlgorithmInterface(unit);
    }

    public void ContextInterface()
    {
        strategy.AlgorithmInterface();
    }
}

/// <summary>
/// Подлечить выбранный дружественный
/// </summary>
public class ConcreteStrategyHealth : Strategy
{
    public override void AlgorithmInterface(Unit unit)
    {
        if (unit.unitSide == UnitSide.PlayerSide)
        {
            unit.allUnitHp = unit.unitHp * unit.originalUnitValue;
            unit.UpdateUnit();
        }
    }
}

/// <summary>
/// Нанести урон выбранному вражескому отряд
/// </summary>
public class ConcreteFrustration : Strategy
{
    public override void AlgorithmInterface(Unit unit)
    {
        if (unit.unitSide == UnitSide.EnemySide)
        {
            unit.allUnitHp = unit.allUnitHp - (unit.allUnitHp/5);
            unit.UpdateUnit();
        }
    }
}

/// <summary>
/// Нанести урон выбранному вражескому отряд
/// </summary>
public class ConcreteDefeat : Strategy
{
    public override void AlgorithmInterface()
    {
        foreach (Unit unit in AttackHelper.Instance.GetAllUnits())
        {
            unit.allUnitHp = unit.allUnitHp - (unit.allUnitHp/3);
            unit.UpdateUnit();
        }
    }
}

public class HeroSkills : MonoBehaviour
{
    public Context context;

    public void InitSkills(Unit unit)
    {
        context.ContextInterface(unit);
        context.ContextInterface();
    }
}

