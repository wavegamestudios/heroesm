﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Загрузить в юнитов их скилы
/// </summary>
public class LoadUnitSkills : Singleton<LoadUnitSkills>
{
    public ActiveSkillEn activeSkill { private get; set; }

    //При выборе скила загружаем соответсвующий префаб с навыком
    public void ActionPlayerClick()
    {
        foreach (Unit unit in AttackHelper.Instance.GetAllUnits())
        {
            Context context = null;

                switch (activeSkill)
                {
                    case ActiveSkillEn.healing:
                        if (unit.unitSide == UnitSide.PlayerSide)
                            context = new Context(new ConcreteStrategyHealth());
                        break;
                    case ActiveSkillEn.frustration:
                        if (unit.unitSide == UnitSide.EnemySide)
                            context = new Context(new ConcreteFrustration());
                        break;
                    case ActiveSkillEn.defeat:
                        context = new Context(new ConcreteDefeat());
                        break;
                }

                if (context != null)
                    LoadSkill(context, unit);
            
        }

        activeSkill = ActiveSkillEn.none;
    }


    private void LoadSkill(Context context, Unit unit)
    {
        GameObject heroSkillsInst = new GameObject();
        heroSkillsInst.transform.SetParent(unit.transform, false);
        heroSkillsInst.name = activeSkill.ToString();
        heroSkillsInst.AddComponent<HeroSkills>().context = context;
        heroSkillsInst.AddComponent<SkillsActivator>();
        Image skillIm = heroSkillsInst.AddComponent<Image>();
        skillIm.sprite = Resources.Load<Sprite>("Images/" + activeSkill.ToString());
        skillIm.preserveAspect = true;
    }
}



