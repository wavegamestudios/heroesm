﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Добавляем на панель активную кнопку скила при селекте юнита
/// </summary>
public class ActivateSkillsButton : Singleton<ActivateSkillsButton>
{
    Transform skillPanel { get; set; }
    public static Action<ActiveSkillEn> InitSkills;

    public ActivateSkillsButton()
    {
        //Трансформ панели активного навыка
        skillPanel = GameObject.Find("PlActiveSkill").transform;
    }

    public void LoadButtonSkills(Unit unit)
    {
        DeleteOldSkillButton();

        //Загрузить кнопку скилов в панель если есть скилл
        ActiveSkillEn activeSkill = unit.activeSkills;

        //Добавить новую кнопку скила
        if (activeSkill != ActiveSkillEn.none && unit.unitSide == UnitSide.PlayerSide)
        {
            GameObject instance = MonoBehaviour.Instantiate(Resources.Load("SkillsButton", typeof(GameObject))) as GameObject;
            instance.transform.SetParent(skillPanel, false);
            instance.name = activeSkill.ToString();

            Image skillIm = instance.GetComponent<Image>();
            skillIm.sprite = Resources.Load<Sprite>("Images/" + activeSkill.ToString());
            skillIm.preserveAspect = true;
            LoadUnitSkills.Instance.activeSkill = activeSkill;

            SkillButton skillButton = instance.GetComponent<SkillButton>();
            skillButton.SetDescription(activeSkill.ToString());
        }
    }

    public void DeleteOldSkillButton()
    {
        //удалить все скилы у юнитов
        foreach (var typeUnit in GameObject.FindObjectsOfType<SkillButton>())
        {
            MonoBehaviour.Destroy(typeUnit.gameObject);
        }
    }
}

