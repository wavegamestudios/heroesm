﻿using UnityEngine;

/// <summary>
/// Снять выделения с селектора
/// </summary>
public class DestSelected : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        AttackHelper.DestroySelected += Destroy;
    }

    private void Destroy(UnitSide unitSide)
    {
        if (unitSide == transform.parent.GetComponent<Unit>().unitSide)
        {
            AttackHelper.DestroySelected -= Destroy;
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        AttackHelper.DestroySelected -= Destroy;
    }
}
