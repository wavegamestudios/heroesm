﻿using DG.Tweening;

public class AttackController : Singleton<AttackController>
{
    Player player { get; set; }
    Enemy enemy { get; set; }

    public void LoadAttack(Player player, Enemy enemy)
    {
        this.player = player;
        this.enemy = enemy;
        DOVirtual.DelayedCall(1.5f, InitController);
    }  

    private void InitController()
    {
        SceneController.instance.LoadScene(MapСatalog.WarMap.ToString());
        WarMap.instance.InitMap(player, enemy);
    }
}
