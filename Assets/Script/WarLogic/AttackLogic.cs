﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Логика ходов и атаки
/// </summary>
public class AttackLogic : MonoBehaviour
{
    Unit enemyActiveUnit { get; set; }
    Unit playerActiveUnit { get; set; }

    public GameObject prefabSelTarget;
    public GameObject prefabSelPlayer;

    public Text winText;
    public Text actionHitText;

    private void OnEnable ()
    {
        ButtonController.clickActionPlayer += ActionPlayerClick;
        UnitTrigger.clickSelectedUnit += ClickSelectedUnit;
        WarMap.selectedPlayer += SelectedPlayer;
        SkillsActivator.CheckAfterSkills += CheckFinis;
    }

    private void OnDisable()
    {
        ButtonController.clickActionPlayer -= ActionPlayerClick;
        UnitTrigger.clickSelectedUnit -= ClickSelectedUnit;
        WarMap.selectedPlayer -= SelectedPlayer;
        SkillsActivator.CheckAfterSkills -= CheckFinishAfterSkill;
    }

    public void Each<T>(IEnumerable<T> items, Action<T> action)
    {
        foreach (var item in items) action(item);
    }

    public Unit QueuePlayerStep(List<Unit> queueUnit)
    {
        var activeUnit = queueUnit.Find(r => r.statusStep == false);

        if (activeUnit == null)
        {
            Each(queueUnit, unit => unit.statusStep = false);
            return queueUnit[0];
        }

        return activeUnit;
    }

    public void ClickSelectedUnit(Unit unit)
    {
        if (unit.unitSide == UnitSide.EnemySide)
        {
            SelectedEnemy(unit);
        }
    }

    public void SelectedPlayer()
    {
        if (AttackHelper.Instance.GetAllUnits().Count > 0)
        {
            playerActiveUnit = QueuePlayerStep(AttackHelper.Instance.GetAllPlayerLive());
            AttackHelper.Instance.SelectedUnit(prefabSelPlayer, playerActiveUnit);
            ActivateSkillsButton.Instance.LoadButtonSkills(playerActiveUnit);
        }
    }

    public void SelectedEnemy(Unit enemuyUnit)
    {
        if (enemuyUnit.statusLive == true)
        {
            enemyActiveUnit = enemuyUnit;
            AttackHelper.Instance.DeSelectUnitSide(UnitSide.EnemySide);
            AttackHelper.Instance.SelectedUnit(prefabSelTarget, enemyActiveUnit);
        }
    }

    private void PlayerAttack()
    {
        int ranAction = AttackHelper.Instance.GetRandomAction(playerActiveUnit.allUnitAction);
        playerActiveUnit.statusStep = true;
        enemyActiveUnit.allUnitHp -= ranAction;
        enemyActiveUnit.UpdateUnit();
        enemyActiveUnit = null;

        ActionText(ranAction.ToString());
        AttackHelper.Instance.ClearAllSelected();
    }

    private void EnemyAttack()
    {      
        if (AttackHelper.Instance.GetAllEnemyLive().Count > 0)
        {
            AttackHelper.Instance.ClearAllSelected();

            var selectedPlayer = AttackHelper.Instance.SelectedRandomPlayerLive();
            AttackHelper.Instance.SelectedUnit(prefabSelTarget, selectedPlayer);

            var selectedStepEnemy = QueuePlayerStep(AttackHelper.Instance.GetAllEnemyLive());
            AttackHelper.Instance.SelectedUnit(prefabSelPlayer, selectedStepEnemy);

            DOVirtual.DelayedCall(1.0f, () =>
            {
                int ranAction = AttackHelper.Instance.GetRandomAction(selectedStepEnemy.allUnitAction);
                selectedPlayer.allUnitHp -= ranAction;
                selectedPlayer.UpdateUnit();
                ActionText(ranAction.ToString());
            }
         );
       }
    }

    public void ActionPlayerClick()
    {
        if (enemyActiveUnit != null && playerActiveUnit != null && AttackHelper.Instance.GetAllPlayerLive().Count > 0)
        {
            PlayerAttack();
            EnemyAttack();

            DOVirtual.DelayedCall(2.0f, () =>
            {
                AttackHelper.Instance.ClearAllSelected();
                ActivateSkillsButton.Instance.DeleteOldSkillButton();
                ActivateSkillsButton.Instance.LoadButtonSkills(playerActiveUnit);
                AttackHelper.Instance.DeleteAllSkills();

                if (AttackHelper.Instance.GetAllPlayerLive().Count > 0)
                {
                    SelectedPlayer();
                }
                CheckFinis();             
            });
        }
    }

    public void CheckFinishAfterSkill()
    {
        ActivateSkillsButton.Instance.DeleteOldSkillButton();

        CheckFinis();
    }

    #region Finish
    private void CheckFinis()
    {                 
        if (AttackHelper.Instance.GetAllEnemyLive().Count <= 0)
        {
            SaveManager.Instance.armyDeathList.Add(WarMap.instance.EnemyArmyName);

            if (SaveManager.Instance.armyDeathList.Count == EnemyController.instance.enemyList.Count)
            {
                Finis("You win");
            }
            else
            {
                SaveManager.Instance.SavePlayerUnits(AttackHelper.Instance.GetAllPlayerLive());
                Finis("Enemy death");
            }
        }

        if (AttackHelper.Instance.GetAllPlayerLive().Count <= 0)
        {
            playerActiveUnit = null;
            WinText("You lose");
        }

    }

    private void Finis(string message)
    {
        WinText(message);

        AttackHelper.Instance.DeleteAllSkills();
        ActivateSkillsButton.Instance.DeleteOldSkillButton();
        StartCoroutine(DoCheck());
    }

    IEnumerator DoCheck()
    {
        yield return new WaitForSeconds(2.0f);
        AttackHelper.Instance.DestroyAllUnits();
        WinText("");
        SceneController.instance.LoadScene(MapСatalog.MainMap.ToString());
    }

    #endregion
    #region UI
    private void ActionText(string action)
    {
        DOVirtual.DelayedCall(2.0f, () =>
        {
            actionHitText.text = "Action " + action;
            actionHitText.text = "";
        });      
    }
    private void WinText(string message)
    {
        winText.text = message;
    }
    #endregion
}


