﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttackHelper : Singleton<AttackHelper>
{
    public static Action<UnitSide> DestroySelected;
    // <summary>
    /// Убрать селектор с определенной стороны
    /// </summary>
    /// <param name="unit"></param>
    public void DeSelectUnitSide(UnitSide unitSide)
    {
        DestroySelected?.Invoke(unitSide);
    }
    public void ClearAllSelected()
    {
        DeSelectUnitSide(UnitSide.PlayerSide);
        DeSelectUnitSide(UnitSide.EnemySide);
    }
    public List<Unit> GetAllUnits()
    {
        return MonoBehaviour.FindObjectsOfType<Unit>().ToList();
    }
    public List<Unit> GetPlayerUnits()
    {
        return GetUnitsQueue(UnitSide.PlayerSide);
    }
    public List<Unit> GetEnemyUnits()
    {
        return GetUnitsQueue(UnitSide.EnemySide);
    }
    /// <summary>
    /// Удалить все юнитов
    /// </summary>
    public void DestroyAllUnits()
    {
        foreach (var unit in GetAllUnits())
        {
            MonoBehaviour.Destroy(unit.gameObject);
        }
    }
    /// <summary>
    /// Удалит все скилы префабы у юнитов
    /// </summary>
    public void DeleteAllSkills()
    {
        //удалить все скилы у юнитов
        foreach (var typeUnit in GameObject.FindObjectsOfType<HeroSkills>())
        {
            MonoBehaviour.Destroy(typeUnit.gameObject);
        }
    }
    /// <summary>
    /// Обновить данные юнитов
    /// </summary>
    public void UpdateAllUnits()
    {
        foreach (Unit unit in GetAllUnits())
        {
            unit.UpdateUnit();
        }
    }
    /// <summary>
    /// Активировать скил юнита
    /// </summary>
    /// <param name="unit"></param>
    public bool ActivateSkills(Unit unit)
    {
        bool complite = false;

        foreach (Transform child in unit.transform)
        {
            if (child.GetComponent<HeroSkills>())
            {
                child.GetComponent<HeroSkills>().InitSkills(unit);
                complite = true;
               
            }
        }

        AttackHelper.Instance.DeleteAllSkills();
        return complite;
    }
    /// <summary>
    /// Выбрать юнита (добавить селектор)
    /// </summary>
    public void SelectedUnit(GameObject prefabForSelec, Unit unit)
    {
        MonoBehaviour.Instantiate(prefabForSelec).transform.SetParent(unit.transform, false);
    }
    /// <summary>
    /// Найти юнитов на определеной стороне
    /// </summary>
    /// <param name="unitSide"></param>
    /// <returns></returns>
    public List<Unit> GetUnitsQueue(UnitSide unitSide)
    {
        return (from Unit unit in MonoBehaviour.FindObjectsOfType<Unit>() where unit.unitSide == unitSide select unit).ToList();
    }
    /// <summary>
    /// Выбираем рандомно живого юнита плеера
    /// </summary>
    /// <returns></returns>
    public Unit SelectedRandomPlayerLive()
    {
        var unitsPlayerLive = GetAllPlayerLive();
        int randomPlayer = UnityEngine.Random.Range(0, unitsPlayerLive.Count - 1);
        Unit selectedPlayer = unitsPlayerLive[randomPlayer];
        return selectedPlayer;
    }
    /// <summary>
    /// Выбираем всех живых противников
    /// </summary>
    /// <returns></returns>
    public List<Unit> GetAllEnemyLive() => GetEnemyUnits().FindAll(r => r.statusLive == true).ToList();
    /// <summary>
    /// Выбрать все живых юнитов игрока
    /// </summary>
    /// <returns></returns>
    public List<Unit> GetAllPlayerLive() => GetPlayerUnits().FindAll(r => r.statusLive == true);
    /// <summary>
    /// Выбираем противников которые не сходили
    /// </summary>
    /// <returns></returns>
    public Unit GetAllNotStepEnemy() => GetAllEnemyLive().Find(r => r.statusStep == false);
    /// <summary>
    /// Выбираем юнитов плеера которые не сходили
    /// </summary>
    /// <returns></returns>
    public Unit GetAllNotStepPlayer() => GetPlayerUnits().Find(r => r.statusStep == false);
    /// <summary>
    /// Рандомная аттака
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public int GetRandomAction(int action) => (int)UnityEngine.Random.Range(0.0f, action);
}
