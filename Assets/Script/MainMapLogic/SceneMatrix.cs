﻿using System;
using System.Linq;
using UnityEngine;


/// <summary>
/// Создание поля матрицы согласно плану карты
/// </summary>
public class SceneMatrix : MonoBehaviourSingleton<SceneMatrix>
{
    public HexCell cellPrefab = null;

    [HideInInspector]
    public HexCell[] cells;

    public void Awake()
    {
        cells = new HexCell[MatrixMapRoad.height * MatrixMapRoad.width];

        for (int z = 0, i = 0; z < MatrixMapRoad.height; z++)
        {
            for (int x = 0; x < MatrixMapRoad.width; x++)
            {
                InitRoadMap(x, z, i++);
            }
        }

        InitAllheighbor();
    }

    /// <summary>
    /// Создаем карту клеток
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <param name="i"></param>
    private void InitRoadMap(int x, int z, int i)
    {
        HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
        cell.transform.SetParent(transform, false);
        cell.transform.localScale = new Vector3(1, 1, 1);
        cell.transform.name = z + ":" + x;

        //Позиция клетки в массиве
        cell.positionCell = new Vector2(z, x);

        if (MatrixMapRoad.roadMap[z, x] == 0)
        {
            cell.OffCell();
        }
    }

    /// <summary>
    /// Найти всеx соседей и добавить в клетку
    /// </summary>
    private void InitAllheighbor()
    {
        foreach (HexCell cell in cells)
        {
            //Добавлять в соседи только активные клетки
            if (cell.activeCell)
            {
                var neighbours = from x in Enumerable.Range(0, MatrixMapRoad.roadMap.GetLength(0)).Where(x => Math.Abs(x - cell.positionCell[0]) <= 1)
                                 from z in Enumerable.Range(0, MatrixMapRoad.roadMap.GetLength(1)).Where(z => Math.Abs(z - cell.positionCell[1]) <= 1)
                                 select Array.Find(cells, item => item.positionCell == new Vector2(x, z));

                foreach (var neighbour in neighbours)
                {
                    if (neighbour.activeCell && cell.positionCell != neighbour.positionCell)
                        cell.SetNeighbor(neighbour);
                }
            }
        }
    }
}
