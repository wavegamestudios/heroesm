﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

enum HideCell : int
 {
    none = 0,
    OFF = 1,
    ON = 2,
};

/// <summary>
/// Тригер клетки ()
/// </summary>
public class TrigerCell : MonoBehaviour, IPointerClickHandler
{
    private HexCell hexCell { get; set; }

    // Normal raycasts do not work on UI elements, they require a special kind
    GraphicRaycaster raycaster;

    Canvas canvas { get; set; }

    void Awake()
    {
        PathController.Instance.OnPlayerEndPatn += CellUpdate;

        canvas = GetComponent<Canvas>();
        // Get both of the components we need to do this
        this.raycaster = GetComponent<GraphicRaycaster>();
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.clickCount == 1)
        {
            PathController.Instance.DeSelectedPath();
            PathController.Instance.SelectedPath(GetComponent<HexCell>());
        }
    }

    private void CellUpdate() => canvas.sortingOrder = (transform.childCount > 0) ? canvas.sortingOrder = (int)HideCell.ON : canvas.sortingOrder = (int)HideCell.OFF;
}
