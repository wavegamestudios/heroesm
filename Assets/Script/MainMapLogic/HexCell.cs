﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Позиция клетки и ее соседей
/// </summary>
public class HexCell : MonoBehaviour
{
    //Позиция клетки в  массиве
    public Vector2 positionCell = new Vector2(0, 0);

    //Сохранить начальный цвет клетки
    Color parentColor = new Color();

    //Активна клетка или нет
    public bool activeCell = true;

    //Список родствеников клетки
    public List<HexCell> neighborsList = new List<HexCell>();

    private void Awake() => parentColor = transform.GetComponent<Image>().color;
    
    public List<HexCell> GetAllNeighbor() => neighborsList;
    public void SetNeighbor(HexCell cell) => neighborsList.Add(cell);
    
    /// <summary>
    /// Отключить изображение и тригер
    /// </summary>
    public void OffCell()
    {
        transform.GetComponent<Image>().enabled = false;
        transform.GetComponent<TrigerCell>().enabled = false;
        transform.GetComponent<Canvas>().enabled = false;
        transform.GetComponent<GraphicRaycaster>().enabled = false;
        activeCell = false;
    }

    /// <summary>
    /// Подсветить клетку
    /// </summary>
    public void SelectedCell() => transform.GetComponent<Image>().color = Color.blue;
    public void DeSelectedCell() => transform.GetComponent<Image>().color = parentColor;  
}