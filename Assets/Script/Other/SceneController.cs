﻿using UnityEngine;

public class SceneController : SingletonBehaviour<SceneController>
{
    public GameObject mainMap = null;
    public GameObject warMap = null;

    public void LoadScene(string sceneName)
    {
        if (mainMap.activeSelf) mainMap.SetActive(false);
        if (warMap.activeSelf) warMap.SetActive(false);
        if (sceneName == "MainMap") mainMap.SetActive(true);
        if (sceneName == "WarMap") warMap.SetActive(true);
    }
}
