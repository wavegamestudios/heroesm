﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Определиния сторон юнитов
/// </summary>
public enum UnitSide {PlayerSide, EnemySide}

/// <summary>
/// Каталог карт
/// </summary>
public enum MapСatalog {MainMap, WarMap}

/// <summary>
/// Активный навыки (frustration -  Урон отряду, defeat - Урон всем, healing - Лечение)
/// </summary> 
public enum ActiveSkillEn  { none, frustration, defeat, healing }


