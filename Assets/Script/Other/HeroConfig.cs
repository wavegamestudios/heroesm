﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroConfig : MonoBehaviour
{
    public string ArmyName = "";

    private void OnEnable()
    {
        //Если герой есть в списке погибших то удаляем
        if (SaveManager.Instance.armyDeathList.Contains(ArmyName))
        {
            transform.GetComponent<Enemy>().enabled = false;
            gameObject.SetActive(false);            
        }
    }
}
