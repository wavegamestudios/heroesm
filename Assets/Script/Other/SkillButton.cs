﻿using UnityEngine;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour
{
    public Text textPrefab;

    /// <summary>
    /// Добавить описание кнопки
    /// </summary>
    /// <param name="text"></param>
    public void SetDescription(string text)
    {
        textPrefab.text = text;
    }
}


