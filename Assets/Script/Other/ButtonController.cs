﻿using System;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public static Action clickActionPlayer;

    //Ход игрока
    public void ClickMovePlayer() => PathController.Instance.MoveLastSelectedCell();
    
    //Атака игрока
    public void ClickActionPlayer() => clickActionPlayer?.Invoke();

    //Загрузить префабы атаки в юнитов
    public void ClickActiveSkills()
    {
        LoadUnitSkills.Instance.ActionPlayerClick();
    }
}
