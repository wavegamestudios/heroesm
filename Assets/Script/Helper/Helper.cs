﻿using System.Linq;
using UnityEngine;

public class Helper : Singleton<Helper>
{
    public HexCell GetCellToPosition(Vector2 positionCell) => (from HexCell cell in SceneMatrix.Instance.cells where cell.positionCell == positionCell select cell).First();
}
