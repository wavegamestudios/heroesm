﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitTrigger : MonoBehaviour, IPointerClickHandler
{
    public static Action<Unit> clickSelectedUnit;

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        clickSelectedUnit?.Invoke(transform.GetComponent<Unit>());
    }
}
