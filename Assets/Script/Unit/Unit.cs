﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Логика юнита
/// </summary>
[SerializeField]
public class Unit : MonoBehaviour
{
    WarriorSkills warriorSkills { get; set; }
    public Text TopText; //Текст для общего количества жизней и атаки
    public Text BottomText; //Текс для колличества юнитов

    public int unitValue { get; set; }
    public string unitName { get; set; }
    public UnitSide unitSide { get; set; } // На какой стороне играет юнит
    public int unitHp { get; set; } // колличество жизней одного юнита
    public int unitAction { get; set; } //атака одного юнита
    public int allUnitHp { get; set; } //суммарное колличество жизней отряда
    public int allUnitAction { get; set; } //суммарная аттака всего отряда
    public bool statusLive = true; //"false" - deadth "true" - live
    public bool statusStep = false; // Ходил юнит или нет
    public string armyName { get; set; } //Армия к которой относится юнит
    public ActiveSkillEn activeSkills; // Активный навок
    public int originalUnitValue { get; private set; } //изначальные данные отряда
    public bool useSkill { get; set; } //Использовал скилл

    private void Start()
    {
        originalUnitValue = unitValue;
        InitSkills();
        UpdateUnit();
    }

    private void InitSkills()
    {
        warriorSkills = (WarriorSkills)ArmyConfig.Instance.WarriorsSkillTable[unitName];
        unitHp = warriorSkills.hp;
        unitAction = warriorSkills.action;
        allUnitHp = GetAllHp();
        allUnitAction = GetAllAction();
        activeSkills = warriorSkills.science;
    }

    public void UpdateUnit()
    {
        unitValue = allUnitHp / unitHp;
        UpdateUnitTextHp();
        UpdateUnitTextValue();
        if (unitValue <= 0) UnitIsDeath();
    }

    public void UnitIsDeath()
    {
        statusLive = false;
        TopText.text = "-";
        BottomText.text = "DEATH";
        allUnitHp = 0;
        allUnitAction = 0;
    }

    public ActiveSkillEn GetActiveSkills() => (unitSide == UnitSide.EnemySide) ? ActiveSkillEn.none: activeSkills; 
    public int GetAllHp() => unitHp * unitValue;
    public int GetAllAction() => unitAction * unitValue;
    private void UpdateUnitTextHp() => TopText.text = "At " + GetAllAction() + "/" + "Hp " + allUnitHp;
    private void UpdateUnitTextValue() => BottomText.text = "Val " + unitValue.ToString();
}
