﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Graphs
{
    class Node
    {
        public string Name { get; }
        public List<Node> Children { get; }

        public Node(string name)
        {
            Name = name;
            Children = new List<Node>();
        }

        public Node AddChildren(Node node, bool bidirect = true)
        {
            Children.Add(node);
            if (bidirect)
            {
                node.Children.Add(this);
            }
            return this;
        }

        public void Handler()
        {
            //Debug.Log("visited  " + this.Name);
        }
    }
}