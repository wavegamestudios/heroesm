﻿using UnityEngine;

/// <summary>
/// Класс для работы с персонажем игрока
/// </summary>
public class PlayerController : SingletonBehaviour<PlayerController>
{
    public Player charPrefab = null;

    [HideInInspector]
    public Player playerInstance = null;

    // Start is called before the first frame update
    void Start() => LoadPlayer();
    
    private void LoadPlayer()
    {
        playerInstance = Instantiate<Player>(charPrefab);
        playerInstance.transform.SetParent(Helper.Instance.GetCellToPosition(CharacterMap.firstCharacterPos).transform, false);
        playerInstance.transform.localPosition = Vector2.zero;
    }

}
