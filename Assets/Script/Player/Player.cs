﻿using DG.Tweening;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Transform GetCellParentTransform() => transform.parent;

    public void MovePlayer(HexCell FinisHexCell)
    {
        transform.SetParent(FinisHexCell.transform, false);
        transform.DOMove(FinisHexCell.transform.position, 0.3f);
    }
}

